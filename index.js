const express = require('express');

const app = express();

const port = 8000;

class Drink {
    constructor(id, maNuocUong, tenNuocUong, donGia, ngayTao, ngayCapNhat) {
        this.id = id;
        this.maNuocUong = maNuocUong;
        this.tenNuocUong = tenNuocUong;
        this.donGia = donGia;
        this.ngayTao = ngayTao;
        this.ngayCapNhat = ngayCapNhat;
    };
};

const drink1 = new Drink(1, 'TRATAC', 'Trà tắc', 10000, '14/5/2021', '14/5/2021');
const drink2 = new Drink(2, 'COCA', 'Cocacola', 15000, '14/5/2021', '14/5/2021');
const drink3 = new Drink(3, 'PEPSI', 'Pepsi', 15000, '14/5/2021', '14/5/2021');

const drinkArr = ['TRATAC', 'COCA', 'PEPSI'];

app.get('/drinks-class', (req, res) => {
    res.json(drinkArr);
});

app.get('/drinks-object', (req, res) => {
    res.json([drink1, drink2, drink3]);
});

app.listen(port, () => {
    console.log('server is listening on port:' + port);
});